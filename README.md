# Terraform Module Registry on Gitlab

Welcome to terraform module registry.

This repository contains all the terraform modules that are available in the softcomweb organization.

The modules are organized by the following structure:

```
modules/
    - module-name/
        - README.md
        - main.tf
        - variables.tf
```

# Getting started

Create a new terraform repository in your Gitlab account.
NOTE: When creating the repository, do not use dot(.) or underscore(_) in the repository name because 
this might lead to unwanted errors when using the api call.


## Publishing module to gitlab registry

format:
    https://gitlab.com/api/v4/projects/<PROJECT_ID>/packages/terraform/modules/<MODULE_NAME>/<TARGET_PLATFORM>/<MODULE_VERSION>/file

    * GITLAB_API_V4_URL="https://gitlab.com/api/v4"
    * PROJECT_ID="123456789"
    * MODULE_NAME="gitlab-oidc"
    * TARGET_PLATFORM="aws"
    * MODULE_VERSION="0.0.1"
    * TOKEN="your_gitlab_token"

and to compress your module use the following command:

```
tar -czvf ${TERRAFORM_MODULE_NAME}-${TERRAFORM_TARGET_PLATFORM}-${TERRAFORM_MODULE_VERSION}.tgz -C modules/${MODULE_NAME} .
```

NOTE:
if you have some file in the repository you will want to exclude, use the --exclude flag

example:
    tar -czvf ${TERRAFORM_MODULE_NAME}-${TERRAFORM_TARGET_PLATFORM}-${TERRAFORM_MODULE_VERSION}.tgz -C modules/${MODULE_NAME} --exclude ./.git .

The tar file will be placed locally in the root of the repository.

# Deploying module to gitlab registry

You can publish the module to the gitlab registry using either a PAT or a deploy token

Using PAT

curl --fail-with-body \
    --header "PRIVATE-TOKEN: <your_personal_access_token>" \
    --upload-file ${TERRAFORM_MODULE_NAME}-${TERRAFORM_TARGET_PLATFORM}-${TERRAFORM_MODULE_VERSION}.tgz \
    ${GITLAB_API_V4_URL}/projects/${PROJECT_ID}/packages/terraform/modules/${MODULE_NAME}/${TARGET_PLATFORM}/${MODULE_VERSION}/file

OR Use deploy token

curl --fail-with-body \
    --header "DEPLOY-TOKEN: <deploy_token>" \
    --upload-file ${TERRAFORM_MODULE_NAME}-${TERRAFORM_TARGET_PLATFORM}-${TERRAFORM_MODULE_VERSION}.tgz \
    ${GITLAB_API_V4_URL}/projects/${PROJECT_ID}/packages/terraform/modules/${MODULE_NAME}/${TARGET_PLATFORM}/${MODULE_VERSION}/file
