#providers
terraform {
  required_version = ">= 1.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.47"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.4"
    }
    # gitlab = {
    #   source  = "gitlabhq/gitlab"
    #   version = ">= 3.0"
    # }
  }
}
