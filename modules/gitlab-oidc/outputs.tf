#outputs
output "oidc_provider_arn" {
  description = "OIDC provider ARN"
  value       = try(aws_iam_openid_connect_provider.oidc_provider[0].arn, "")
}

output "oidc_role_arn" {
  description = "CICD GitHub role."
  value       = try(aws_iam_role.oidc_role[0].arn, "")
}

output "policy_document" {
  value = join("", data.aws_iam_policy_document.this[*].json)
}
