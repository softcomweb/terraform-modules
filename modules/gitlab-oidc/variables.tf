variable "environment" {
  type        = string
  description = "Environment (dev|qs|live)"
}

variable "create_oidc_provider" {
  type        = bool
  description = "Controls whether to create OIDC Provider"
  default     = false
}

variable "create_oidc_role" {
  type        = bool
  description = "Controls whether to create OIDC attached role"
  default     = true
}

variable "oidc_role_name" {
  type        = string
  description = "(Optional, forces new resource) role name"
  default     = "gitlab-oidc-role"
}

variable "oidc_role_description" {
  type        = string
  description = "Description of the oidc role"
  default     = "Role assumed by Gitlab oidc provider"
}

variable "gitlab_projects" {
  type        = set(string)
  description = "Names or IDs of the Gitlab projects where product code is held. Can be left unset if static credentials should not be put into Gitlab."
  default     = []
}

variable "gitlab_url" {
  type        = string
  description = "Gitlab url"
  default     = "https://gitlab.com"
}

variable "tags" {
  type        = map(string)
  description = "Tags that are added by default to every resource"
  default     = {}
}
