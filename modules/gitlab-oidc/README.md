# AWS GitLab OIDC Provider Terraform Module

This module allows you to create a GitLab OIDC provider and the associated IAM roles, that will help GitLab Actions to securely authenticate against the AWS API using an IAM role.

We recommend using GitLab's OIDC provider to get short-lived credentials needed for your actions. Specifying role-to-assume without providing an aws-access-key-id or a web-identity-token-file will signal to the action that you wish to use the OIDC provider. The default session duration is 1 hour when using the OIDC provider to directly assume an IAM Role. The default session duration is 6 hours when using an IAM User to assume an IAM Role (by providing an aws-access-key-id, aws-secret-access-key, and a role-to-assume) . If you would like to adjust this you can pass a duration to role-duration-seconds, but the duration cannot exceed the maximum that was defined when the IAM Role was created. The default session name is GitLabActions, and you can modify it by specifying the desired name in role-session-name.

## Use-Cases

1. Retrieve temporary credentials from AWS to access cloud services
1. Use credentials to retrieve secrets or deploy to an environment
1. Scope role to branch or project
1. Create an AWS OIDC provider for GitLab Actions

## Features

2. Create one or more IAM role that can be assumed by GitLab Actions
3. IAM roles can be scoped to :
     * One or more GitLab organisations
     * One or more GitLab repository
     * One or more branches in a repository

| Feature                                                                                                | Status |
|--------------------------------------------------------------------------------------------------------|--------|
| Create a role for all repositories in a specific GitLab organisation                                    | ✅     |
| Create a role specific to a repository for a specific organisation                                       | ✅     |
| Create a role specific to a branch in a repository                                                      | ✅     |
| Create a role for multiple organisations/repositories/branches                                         | ✅     |
| Create a role for organisations/repositories/branches selected by wildcard (e.g. `feature/*` branches) | ✅     |

---

[![linter](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider/actions/workflows/linter.yml/badge.svg)](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider/actions/workflows/linter.yml)
[![release.draft](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider/actions/workflows/release.draft.yml/badge.svg)](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider/actions/workflows/release.draft.yml)

[![](https://img.shields.io/github/license/terraform-module/terraform-aws-gitlab-oidc-provider)](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/github/v/tag/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/issues/github/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/github/issues/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/github/issues-closed/terraform-module/terraform-aws-gitlab-oidc-provider)
[![](https://img.shields.io/github/languages/code-size/terraform-module/terraform-aws-gitlab-oidc-provider)](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider)
[![](https://img.shields.io/github/repo-size/terraform-module/terraform-aws-gitlab-oidc-provider)](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/github/languages/top/terraform-module/terraform-aws-gitlab-oidc-provider?color=green&logo=terraform&logoColor=blue)
![](https://img.shields.io/github/commit-activity/m/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/github/contributors/terraform-module/terraform-aws-gitlab-oidc-provider)
![](https://img.shields.io/github/last-commit/terraform-module/terraform-aws-gitlab-oidc-provider)
[![Maintenance](https://img.shields.io/badge/Maintenu%3F-oui-green.svg)](https://GitHub.com/terraform-module/terraform-aws-gitlab-oidc-provider/graphs/commit-activity)
[![GitHub forks](https://img.shields.io/github/forks/terraform-module/terraform-aws-gitlab-oidc-provider.svg?style=social&label=Fork)](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider)

---

## Documentation

- [TFLint Rules](https://github.com/terraform-linters/tflint/tree/master/docs/rules)

## Usage example

IMPORTANT: The master branch is used in source just as an example. In your code, do not pin to master because there may be breaking changes between releases. Instead pin to the release tag (e.g. ?ref=tags/x.y.z) of one of our [latest releases](https://gitlab.com/tchibo-com/shared/terraform/aws/gitlab-oidc-provider/-/tags).

```hcl
module "gitlab-oidc" {
  source  = "git@gitlab.com:tchibo-com/shared/terraform/aws/gitlab-oidc-provider.git?ref=v1.0.0"
  version = "~> 1"

  create_oidc_provider = true

  gitlab_projects      = ["111222"]
}
```

## Examples

See `examples` directory for working examples to reference

- [Examples TFM Dir](https://github.com/terraform-module/terraform-aws-gitlab-oidc-provider)
- [Examples Gitlab Pipeline](./examples/)

## Assumptions

## Available features

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.47 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 3.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | 4.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.47 |
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 3.0 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 4.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.oidc_provider](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_role.oidc_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [gitlab_project_variable.oidc_role_arn](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.gitlab_oidc_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [gitlab_project.projects](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |
| [tls_certificate.gitlab](https://registry.terraform.io/providers/hashicorp/tls/4.0.4/docs/data-sources/certificate) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_oidc_provider"></a> [create\_oidc\_provider](#input\_create\_oidc\_provider) | Controls whether to create OIDC Provider | `bool` | `false` | no |
| <a name="input_create_oidc_role"></a> [create\_oidc\_role](#input\_create\_oidc\_role) | Controls whether to create OIDC attached role | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment (dev\|qs\|live) | `string` | n/a | yes |
| <a name="input_gitlab_projects"></a> [gitlab\_projects](#input\_gitlab\_projects) | Names or IDs of the Gitlab projects where product code is held. Can be left unset if static credentials should not be put into Gitlab. | `set(string)` | `[]` | no |
| <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url) | Gitlab url | `string` | `"https://gitlab.com"` | no |
| <a name="input_oidc_role_description"></a> [oidc\_role\_description](#input\_oidc\_role\_description) | Description of the oidc role | `string` | `"Role assumed by Gitlab oidc provider"` | no |
| <a name="input_oidc_role_name"></a> [oidc\_role\_name](#input\_oidc\_role\_name) | (Optional, forces new resource) role name | `string` | `"gitlab-oidc-role"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that are added by default to every resource | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_oidc_provider_arn"></a> [oidc\_provider\_arn](#output\_oidc\_provider\_arn) | OIDC provider ARN |
| <a name="output_oidc_role_arn"></a> [oidc\_role\_arn](#output\_oidc\_role\_arn) | CICD GitHub role. |
| <a name="output_policy_document"></a> [policy\_document](#output\_policy\_document) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->


### :memo: Guidelines

 - :memo: Use a succinct title and description.
 - :bug: Bugs & feature requests can be be opened
 - :signal_strength: Support questions are better asked on [Stack Overflow](https://stackoverflow.com/)
 - :blush: Be nice, civil and polite ([as always](http://contributor-covenant.org/version/1/4/)).

## License

Copyright 2019 Ivan Katliarhcuk

MIT Licensed. See [LICENSE](./LICENSE) for full details.

## How to Contribute

Submit a pull request

## Terraform Registry

- [Module](https://registry.terraform.io/modules/terraform-module/gitlab-oidc-provider/aws/latest)

## Resources

- [AWS: create oidc](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_providers_create_oidc.html)
- [Blog: OIDC with AWS and GitLab](https://oblcc.com/blog/configure-openid-connect-for-gitlab-and-aws/)
- [Blog: Gitlab OIDC](https://docs.gitlab.com/ee/ci/cloud_services/aws/)
- [Tfm: OIDC Gitlab](https://gitlab.com/guided-explorations/aws/configure-openid-connect-in-aws/)

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.47 |
| <a name="requirement_gitlab"></a> [gitlab](#requirement\_gitlab) | >= 3.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | 4.0.4 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.47 |
| <a name="provider_gitlab"></a> [gitlab](#provider\_gitlab) | >= 3.0 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 4.0.4 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.oidc_provider](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_role.oidc_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [gitlab_project_variable.oidc_role_arn](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/project_variable) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.gitlab_oidc_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [gitlab_project.projects](https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/data-sources/project) | data source |
| [tls_certificate.gitlab](https://registry.terraform.io/providers/hashicorp/tls/4.0.4/docs/data-sources/certificate) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_create_oidc_provider"></a> [create\_oidc\_provider](#input\_create\_oidc\_provider) | Controls whether to create OIDC Provider | `bool` | `false` | no |
| <a name="input_create_oidc_role"></a> [create\_oidc\_role](#input\_create\_oidc\_role) | Controls whether to create OIDC attached role | `bool` | `true` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | Environment (dev\|qs\|live) | `string` | n/a | yes |
| <a name="input_gitlab_projects"></a> [gitlab\_projects](#input\_gitlab\_projects) | Names or IDs of the Gitlab projects where product code is held. Can be left unset if static credentials should not be put into Gitlab. | `set(string)` | `[]` | no |
| <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url) | Gitlab url | `string` | `"https://gitlab.com"` | no |
| <a name="input_oidc_role_description"></a> [oidc\_role\_description](#input\_oidc\_role\_description) | Description of the oidc role | `string` | `"Role assumed by Gitlab oidc provider"` | no |
| <a name="input_oidc_role_name"></a> [oidc\_role\_name](#input\_oidc\_role\_name) | (Optional, forces new resource) role name | `string` | `"gitlab-oidc-role"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | Tags that are added by default to every resource | `map(string)` | `{}` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_oidc_provider_arn"></a> [oidc\_provider\_arn](#output\_oidc\_provider\_arn) | OIDC provider ARN |
| <a name="output_oidc_role_arn"></a> [oidc\_role\_arn](#output\_oidc\_role\_arn) | CICD GitHub role. |
| <a name="output_policy_document"></a> [policy\_document](#output\_policy\_document) | n/a |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->