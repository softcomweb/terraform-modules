terraform {
  required_providers {
    local = {
      source = "hashicorp/local"
      version = "2.4.0"
    }
    aws = {
      source = "hashicorp/aws"
      version = "5.35.0"
    }
  }
}

provider "aws" {
  region = "eu-central-1"
  access_key = "mock"
  secret_key = "mock"
  s3_use_path_style = true
  #s3_force_path_style = false
  skip_credentials_validation = true
  skip_metadata_api_check = true
  skip_requesting_account_id = true
  insecure = true

  endpoints {    
    acm = "http://localhost:4566"
    apigateway = "http://localhost:4566"
    cloudformation = "http://localhost:4566"
    cloudwatch = "http://localhost:4566"
    config = "http://localhost:4566"
    dynamodb = "http://localhost:4566"
    ec2 = "http://localhost:4566"
    es = "http://localhost:4566"
    events = "http://localhost:4566"
    firehose = "http://localhost:4566"
    iam = "http://localhost:4566"
    kinesis = "http://localhost:4566"
    kms = "http://localhost:4566"
    lambda = "http://localhost:4566"
    logs = "http://localhost:4566"
    opensearch = "http://localhost:4566"
    redshift = "http://localhost:4566"
    resourcegroups = "http://localhost:4566"
    resourcegroupstaggingapi = "http://localhost:4566"
    route53 = "http://localhost:4566"
    route53resolver = "http://localhost:4566"
    s3 = "http://localhost:4566"
    #s3             = "http://s3.localhost.localstack.cloud:4566"
    s3control = "http://localhost:4566"
    secretsmanager = "http://localhost:4566"
    ses = "http://localhost:4566"
    sns = "http://localhost:4566"
    sqs = "http://localhost:4566"
    ssm = "http://localhost:4566"
    stepfunctions = "http://localhost:4566"
    sts = "http://localhost:4566"
    swf = "http://localhost:4566"
    transcribe = "http://localhost:4566"
  }
  
  default_tags {
    tags = {
      created_by = "terraform"
      workspace = terraform.workspace
    }
  }
}
