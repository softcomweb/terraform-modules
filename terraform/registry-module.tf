module "my_module_name" {
    source = "gitlab.com/softcomweb/aws-policy/aws"
    version = "0.0.2"

    create = true
    statements = [
        {
            sid = "SoftcomwebAccess"
            actions = [
                "ec2:DescribeTags"
            ]
            effect = "Allow"
            resources = ["*"]

            condition = [
                {
                    test = "ArnLike"
                    variable = "aws:PrincipalArn"
                    values = ["arn:aws:iam::*:role/test-exec"]
                }
            ]
        }
    ]
}

# Using module from local path
module "oidc" {
    source = "../modules/gitlab-oidc"
    create_oidc_provider = true
    environment = "dev"
}

# Using module from gitlab registry
module "gitlab_registry_oidc" {
  source = "gitlab.com/softcomweb/gitlab-oidc/aws"
  version = "0.0.1"
  create_oidc_provider = true
  environment = "live"
}
