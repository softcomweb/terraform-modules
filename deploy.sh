GITLAB_API_V4_URL="https://gitlab.com/api/v4"
PROJECT_ID=54688148
MODULE_NAME="gitlab-oidc"
#TARGET_PLATFORM="aws"
TARGET_PLATFORM="google"
MODULE_VERSION="0.0.2"
GITLAB_TOKEN=$(cat ~/.ssh/private/gitlab-leadel-kolo-token)

curl --fail-with-body \
    --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
    --upload-file ${MODULE_NAME}-${TARGET_PLATFORM}-${MODULE_VERSION}.tgz \
    ${GITLAB_API_V4_URL}/projects/${PROJECT_ID}/packages/terraform/modules/${MODULE_NAME}/${TARGET_PLATFORM}/${MODULE_VERSION}/file
